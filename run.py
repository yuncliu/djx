#!/usr/bin/env python3
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djx.settings")
    from django.core.management import execute_from_command_line

    if not os.path.isfile('db.sqlite3'):
        print('db not exist, migrating...')
        execute_from_command_line(['./manage.py', 'makemigrations', 'forum'])
        execute_from_command_line(['./manage.py', 'migrate'])
    execute_from_command_line(['./manage.py', 'runserver', '0.0.0.0:9999'])
