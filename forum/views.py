from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.template import loader
from django.contrib.staticfiles.views import serve
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from . import models

# Create your views here.
def loginv(request):
    print(request.GET)
    if request.method == 'POST':
        if 'username' in request.POST and 'password'  in  request.POST:
            user = authenticate(username = request.POST['username'], password = request.POST['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                return HttpResponseRedirect("/forum/index")
            else:
                return HttpResponseRedirect('/forum/login?login=failed')
        else:
            if request.user.is_authenticated():
                logout(request)

    result = {}
    if 'login' in request.GET and request.GET['login'] == 'failed':
        result['info'] = 'login failed!'
    else:
        result['info'] = ''

    return render(request, 'login.html', result)

def index(request):
    if request.method == 'GET':
        print(request.user)
        if not request.user.is_authenticated():
            return HttpResponseRedirect('/forum/login')
        items = models.Item.objects.all()
        template = loader.get_template('index.html')
        x = {'items': items}
        return render(request, 'index.html', x)

    if request.method == 'POST':
        c = request.POST['text_input']
        u = request.user.username
        if len(c)>0:
            item = models.Item(content = c, author = u)
            item.save()
        return HttpResponseRedirect("/forum/index")

def signup(request):
    """
    user signup
    """
    if request.method == 'POST':
        if 'username' in request.POST and 'password'  in  request.POST:
            user = User.objects.create_user(request.POST['username'], None, request.POST['password'])
            user.save()
            u = authenticate(username = request.POST['username'], password = request.POST['password'])
            login(request, u)
            return HttpResponseRedirect("/forum/index")

    if request.method == 'GET':
        return render(request, 'signup.html', {})
