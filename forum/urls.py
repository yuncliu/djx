from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.loginv, name='index'),
    url(r'^login$', views.loginv, name='index'),
    url(r'^signup$', views.signup, name='signup'),
    url(r'^index$', views.index, name='index'),
]
