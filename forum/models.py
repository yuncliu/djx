from django.db import models

# Create your models here.
class Author(models.Model):
    full_name = models.CharField(max_length = 100)
    def __str__(self):
        return self.full_name

class Article(models.Model):
    pub_date = models.DateField()
    title = models.CharField(max_length = 100)
    content = models.TextField()
    author = models.ForeignKey(Author, on_delete = models.CASCADE)
    def __str__(self):
        return self.title

class Item(models.Model):
    content = models.TextField()
    author = models.TextField()
    def __str__(self):
        return self.content
